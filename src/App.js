import React, { Component } from 'react';
import './App.css';
import FileUpload from './FileUpload/FileUpload';
import { BrowserRouter, Route } from 'react-router-dom'

import Home from './Home/Home';

class App extends Component {
  state = {
    loading: true
  }

  componentDidMount() {
    this.setState({ loading: false })
  }

  render() {
    const { loading } = this.state;

    if (loading) { // if your component doesn't have to wait for an async action, remove this block 
      return null; // render null when app is not ready
    }

    return (
      <div className="root">
        <div className="page-header header-filter" filter-color="purple">

          <nav className="navbar  navbar-transparent navbar-absolute  navbar-expand-lg " color-on-scroll="100" id="sectionsNav">
            <div className="container">
              <div className="navbar-translate">
                <a className="navbar-brand" href="../index.html"></a>
              </div>
            </div>
          </nav>
          <BrowserRouter>
            <Route path='/' render={props => <FileUpload {...props} />} />
          </BrowserRouter>
        </div>
      </div>
    );
  }
}

export default App;
