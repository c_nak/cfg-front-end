import React from 'react';
import './PermissionUpdateUpload.css';
import axios from 'axios';
import UploadStatus from '../UploadStatus';
import { BASE_URL } from '../Config'

class PermissionUpdateUpload extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      uploadStatus: false,
      data: null,
      statusResult: null
    }
    this.handleUpload = this.handleUpload.bind(this);
  }

  handleUpload(ev) {
    ev.preventDefault();
    var self = this;

    const data = new FormData();
    data.append('file', this.uploadInput.files[0]);
    data.append('permissionGroup', this.permissionGroup.value)

    self.props.isLoading(true)

    if (this.uploadInput.files[0] === undefined) {
      self.props.isLoading(false)
      self.props.onUpdate("Error: Please upload a file")
    }
    else {
      axios.post(BASE_URL + '/updatePermissionById', data)
        .then(function (response) {
          console.log(`Data file -> ${response.status}`);
          self.props.isLoading(false)
          self.props.onUpdate(response.data)
        })
        .catch(function (error) {
          self.props.isLoading(false)
          console.warn(error);
          self.props.onUpdate("Error: Please try again, server error")
        });
    }
  }

  render() {
    return (
      <div className="container file-upload">

        <div className="col-md-12 mr-auto">
          <h2 className="card-title text-center">Update Permissions of students</h2>

          <form onSubmit={this.handleUpload}>
            <div className="form-group">
              <input className="form-control" ref={(ref) => { this.uploadInput = ref; }} type="file" />
              <select title="permissionGroup" className="plain-select permission-combo-box"
                ref={select => this.permissionGroup = select}
              >
                <option value="Corporate">Corporate</option>
                <option value="Community">Community</option>
                <option value="Alumni Students">alumni_students</option>
                <option value="cfg_team">cfg_team</option>
              </select>
            </div>
            <button className="text-center btn brn-primary btn-success confirm-button" type="submit">Upload</button>
          </form>
        </div>
      </div>
    )
  }
}

export default PermissionUpdateUpload;
