import './CourseCompletedUpdateUpload.css';
import React from 'react';
import axios from 'axios';
import UploadStatus from '../UploadStatus';
import { BASE_URL } from '../Config'

class CourseCompletedUpdateUpload extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      uploadStatus: false,
      data: null
    }
    this.handleUpload = this.handleUpload.bind(this);
  }

  handleUpload(ev) {
    ev.preventDefault();
    var self = this;

    const data = new FormData();
    data.append('file', this.uploadInput.files[0]);

    self.props.isLoading(true)

    if (this.uploadInput.files[0] === undefined) {
      self.props.isLoading(false)
      self.props.onUpdate("Error: Please upload a file")
    } else {
      axios.post(BASE_URL + '/upload', data)
        .then(function (response) {
          console.log(`Data file -> ${response.status}`);
          self.props.isLoading(false)
          self.props.onUpdate(response.data)
        })
        .catch(function (error) {
          console.warn(error);
          self.props.isLoading(false)
          self.props.onUpdate("Error: Please try again, server error")
        });
    }
  }

  render() {
    return (
      <div className="container file-upload">
        <h2 className="card-title text-center">Upload students who've completed course</h2>
        <form onSubmit={this.handleUpload}>
          <div className="form-group">
            <input className="form-control" ref={(ref) => { this.uploadInput = ref; }} type="file" />
          </div>

          <button className="btn btn-success" type="submit">Upload</button>
        </form>
      </div>
    )
  }
}


export default CourseCompletedUpdateUpload;
