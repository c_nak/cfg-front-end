import React from 'react';
import './UploadStatus.css';

class UploadStatus extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    const status = this.props.status.replace(/↵/g, "\n"); 
    return (
      <div>
        <div className="col-md-10 ml-auto mr-auto">
          <a href="/" className="btn btn-primary" >BACK to Upload CSV</a>
          <div className="card card-signup container">
            <h2 className="card-title text-center">Upload Status</h2>
            <div className="card-body">
              <div className="row">
                <div className="col-md-12 mr-auto">
                  <p className="message">{status } </p>
                </div>
                <div className="text-center">
                  <h3 className="numberOfSuccessfulImport"></h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default UploadStatus;