import React from 'react';
import './FileUpload.css';
import axios from 'axios';
import UploadStatus from '../UploadStatus/UploadStatus';
import PermissionUpdateUpload from '../PermissionUpdateUpload/PermissionUpdateUpload';
import CourseCompletedUpdateUpload from '../CourseCompletedUpdateUpload/CourseCompletedUpdateUpload';
// import  { Redirect } from 'react-router-dom'
import { BASE_URL } from '../Config'

class FileUpload extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      statusResult: null,
      isLoading: null
    }
  }

  isLoading = (val) => {
    this.setState({
      isLoading: val
    })
  };

  onUpdate = (val) => {
    this.setState({
      statusResult: val
    })
  };

  render() {
    if (this.state.isLoading) {
      return <div className="loader"></div>;
    }

    if (this.state.statusResult !== null) {
      return <UploadStatus status={this.state.statusResult} />
    }

    return (
      <div className="container">
        <div className="col-md-10 ml-auto mr-auto">
          <div className="card card-signup">
            <div className="card-body">
              <div className="row">
                <CourseCompletedUpdateUpload onUpdate={this.onUpdate} isLoading={this.isLoading} />
                <PermissionUpdateUpload onUpdate={this.onUpdate} isLoading={this.isLoading} />

              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default FileUpload;
